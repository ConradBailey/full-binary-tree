#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

const int NO_PARENT = -1; //substitute for a node, which is never negative

int maxTree(const vector<vector<int> >& tree, const int root, const int parent = NO_PARENT) {
// This function recursively calculates the size of a tree, starting at the
// root argument, assuming it's the largest possible full binary tree.
// i.e. all nodes have two or zero children.

// Special care is taken not to recurse backwards into parent nodes

  int children = tree[root].size();
  if (parent != NO_PARENT) children--;

  //base case
  if (children < 2) //The full binary tree cannot go further
    return 1;

  int size = 1;
  if (children > 2) { //we must determine the two largest children
    int left = 0, right = 0;

    for (auto child : tree[root]) {
      if (child == parent) continue; //don't go backwards

      int temp = maxTree(tree, child, root);
      if (temp > left) {
        if (temp > right) {
          left = right;
          right = temp;
        }
        else
          left = temp;
      }
    }
    size += left + right;
  }

  else { //There's only two children, must utilize both
    for (auto child : tree[root]) {
      if (child != parent) { //don't go backwards
        size += maxTree(tree, child, root);
      }
    }
  }

  return size;
}

int main() {
  int numNodes, numCases;
  cin >> numCases;

  for (int t = 0; t < numCases; ++t) {
    cout << "Case #" << t+1 << ": ";

    cin >> numNodes;
    vector<vector<int> > tree (numNodes);
    int nodeA, nodeB;
    for (int edge = 0; edge < (numNodes - 1); ++edge) {
      cin >> nodeA >> nodeB;
      // the tree is zero indexed so we must decrement
      nodeA--; nodeB--;

      // the tree is a vector of vectors of attached nodes
      // e.g. if tree[2] = {0, 4, 13} then that means
      //   node 2 is attached to nodes 0, 4, and 13.
      tree[nodeA].push_back(nodeB);
      //   0, 4, and 13 are also attached to node 2.
      tree[nodeB].push_back(nodeA);
    }

    int maxSize = 0;
    for (int node = 0; node < numNodes; ++node) {
      maxSize = max(maxTree(tree, node), maxSize);
    }

    // output number of nodes that must be deleted
    cout << (numNodes - maxSize) << endl;
  }

  return 0;
}
