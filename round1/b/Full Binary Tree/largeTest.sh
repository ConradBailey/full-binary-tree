#/bin/bash
make
echo
./mySol < input/large > output/large
./snapdragonSol < input/large > output/correct/large
diff -s output/large output/correct/large
echo
/usr/bin/time -v -o time1 ./mySol < input/large > /dev/null
/usr/bin/time -v -o time2 ./snapdragonSol < input/large > /dev/null
diff time1 time2
rm time1 time2
