#/bin/bash
make
echo
./mySol < input/small > output/small
./snapdragonSol < input/small > output/correct/small
diff -s output/small output/correct/small
echo
/usr/bin/time -v -o time1 ./mySol < input/small > /dev/null
/usr/bin/time -v -o time2 ./snapdragonSol < input/small > /dev/null
diff time1 time2
rm time1 time2
