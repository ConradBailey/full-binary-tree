This is my solution to the Full Binary Tree problem from Google's CodeJam (Round 1A, prob B)
Full info here: https://code.google.com/codejam/contest/2984486/dashboard#s=p1

This was my first coding competition, and among my first attempts at timed coding.

I missed a crucial part of the problem, that none of the inputs would have cycles, the inputs would
always be trees. I haven't considered the implications of not including that stipulation, but maybe it
would be fun to investigate.

I didn't finish this problem in the time allotted. Once I realized the bit about no cycles being input,
I understood immediately the conceptual strategy of finding the largest possible tree and outputting
the difference, but I was slow at implementing it.

My first go at it was very naive. I wish I had initialized my git repo then, but I didn't so I'll try
to explain this stage. I had recursive functions for finding branch size, deleting branches, and then
finding the maximum of those sub-trees calculated. It was a fun exercise in recursion and allowed me to
completely conceptualize the problem, but it was sooooo slow. So I downloaded SnapDragon's answer to
compare. Needless to say I embarrassed my self; his program was lightning fast in comparison.

My first step was getting rid of the deleting functions and branch size functions. Instead I had one
function:
     int maxTree(vector<vector<int> >& tree, int root)
It would look at the childrens' lists of connections, delete connections to the parent (to keep the
recursion from going back up the tree), and then recurse into their children, until a node with 1 or 0
children was met.

This was simpler to look at, and a little faster, but still relatively glacial. So I got the idea that
some branches were being traversed multiple times, and if I could store their values in a map then I
could remove substitute the traversals with a map.find(). I'm still unsure of what the cost of a
traversal would have to be to justify that substitution, but I figured it was worth a shot.

After I tried to implement that (the initial commit!) I realized I hadn't accounted for a branches
being traversed from different directions. Besides that, it seemed even slower than before. So I
decided if I continued on this path I'd only add complexity to a strategy that seemed less efficient
than before.

I decided instead to eliminate any modifications to the tree. This would save me from resetting all
those vectors every time I needed to test a note, and all the erase() and find() calls. I knew that
copying vectors like that had been a huge time-sink, but I kept putting off because it didn't seem as
interesting as developing a more elegant algorithm. I really underestimated the time savings though! It
is incredible. As of now (the third and fourth commit) my program runs in a THIRD of the time
SnapDraon's does. Obviously he did his in twenty minutes, but I'm very pleased with how I improved.
Please take a look, I'd like feedback on style, best practices, anything!

Also, I might make a branch where I try that mapping thing still, just to see.
Thanks for reading!